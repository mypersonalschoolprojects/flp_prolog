#! /bin/bash

# Project: FPL second project: Hamiltonian cycle variant
# File:    run_tests.sh
# Usage:   running all prepared test cases
# Author:  David Vodák <xvodak05@stud.fit.vutbr.cz>

DIR_PATH=$(cd $(dirname $0) && pwd)

IN_FILES=$(ls $DIR_PATH/test/*.in)

for F in $IN_FILES;
do
	echo "==================================================================="
	echo "Testing file: " $F
	echo "With content:"
	cat $F
	echo "Output: "
	$DIR_PATH/flp20-log <$F
	echo "==================================================================="
done
