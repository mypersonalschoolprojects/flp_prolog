#
#   FLP 2. project: Hamiltonian cycle
#
# Author: David Vodák <xvodak05@stud.fit.vutbr.cz>
# Login:  xvodak05
# Year:   2021

OUT=flp20-log
SRC=flp.pl
R_TEST=run_tests.sh
TESTS  = test/test0.in test/test1.in test/test2.in
TESTS += test/test2_1.in test/test2_2.in test/test3.in test/test4.in
TESTS += test/test5.in test/test6.in test/test7.in

all:
	swipl -q -g main -o $(OUT) -c $(SRC)
pack:
	zip flp-log-xvodak05.zip $(SRC) Makefile README $(R_TEST) $(TESTS)
run:
	./$(OUT)
run-tests:
	./$(R_TEST)
