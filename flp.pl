%
%   FLP 2. project: Hamiltonian cycle
%
% Author: David Vodák <xvodak05@stud.fit.vutbr.cz>
% Login:  xvodak05
% Year:   2021

:- dynamic edge/2.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          Input parsing                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Predicates for getting the input: read_line, isEOFEOL and readline
% Its author is Martin Hyrs <ihyrs@fit.vutbr.cz> and source code can
% be found in the VUT FIT WIS web pages at:
%	/ Soubory k předmětům / Funkcionální a logické programování
%	/ Projekty / log / input2.pl

read_line(L,C) :-
	get_char(C),
	(isEOFEOL(C), L = [], !;
		read_line(LL,_),
		[C|LL] = L).

isEOFEOL(C) :-
	C == end_of_file;
	(char_code(C,Code), Code==10).

read_lines(Ls) :-
	read_line(L,C),
	( C == end_of_file, Ls = [] ;
	  read_lines(LLs), Ls = [L|LLs]
	).

% Predicate upload_edges: It takes lines which were read from input.
% The lines should look like this:
% A B
% B C
% .....
% Edges of graph which are described on lines will be asserted to
% dynamic predicate called 'edge'.
%
% If the lines are in wrong format they will be skipped
upload_edges([]).
upload_edges([L|Ls]) :-
	L = [A, ' ', B],
	char_type(A, upper), char_type(B, upper),
	\+ undirected_edge(A:B),
	assertz(edge(A,B)),
	upload_edges(Ls).
% Non upper case letters found in line -> skip
upload_edges([L|Ls]) :-
	L = [A, ' ', B],
	\+ char_type(A, upper), char_type(B, upper),
	upload_edges(Ls).
% Wrong input format in line -> skip
upload_edges([L|Ls]) :-
	\+ L = [_, ' ', _],
	upload_edges(Ls).
% There are same edges in the undirected graph -> skip
upload_edges([L|Ls]) :-
	L = [A, ' ', B],
	undirected_edge(A:B),
	upload_edges(Ls).
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             Finding Hamiltonian cycles in parsed input            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Test if the vertices An and Bn are already taken or not
taken_test([], _).
taken_test([H|T], An:Bn) :-
	H \= An,
	H \= Bn,
	taken_test(T, An:Bn).

% The graph is undirected, so it means that edges A-B and B-A are
% the same. This predicate can tell whether one of those edges exists
% (which are basically the same edge).
undirected_edge(An:Bn) :-
	edge(Bn, An) ; edge(An, Bn).

% Find next possible edge, that can be part of the Hamiltonian cycle
next_edge(A:B, An:Bn) :-
	undirected_edge(An:Bn),
	B = An,
	A \= Bn.

% Gen number of vertices in the graph (graph which consist of edges
% in the 'edge' predicate)
get_num_vertices(N) :-
	setof(X, Y^undirected_edge(X:Y), L),
	length(L, N).

% Find Hamiltonian cycle, which starts at the S vertex
find_cycle(S, Res) :-
	undirected_edge(S:SB),
	find_cycle_r(S, [S], S:SB, R),
	Res = [S:SB|R].

% Recursive predicate for finding the Hamiltonian cycle
% Parameters:
%	Start      : starting (and ending) vertex
%	Taken      : list of vertices, which were already taken
%	A:B        : Current edge in the cycle
%	[An:Bn|Res]:
%		An:Bn: Next edge in the cycle
%		Res:   Result (Hamiltonian cycle)
find_cycle_r(Start, Taken, A:B, [An:Bn|Res]) :-
	next_edge(A:B, An:Bn),
	taken_test(Taken, An:Bn),
	Bn \= Start,
	find_cycle_r(Start, [An|Taken], An:Bn, Res).
% End of recursion
find_cycle_r(Start, Taken, A:B, [An:Bn]) :-
	next_edge(A:B, An:Bn),
	Bn = Start,
	get_num_vertices(N),
	length([A|Taken], N).

% The find_cycles predicate can find the same cycle twice.
% The both cycles have same vertices but they have different
% directions. These cycles are for example:
%
% 	A:B B:C C:D
%	D:C C:B B:A
%
% Since this is undirected graph, the cycles are the same and one
% of them has to be removed. It is done by following predicates.
revert_edges([], []).
revert_edges([A:B|T], [B:A|RT]) :- revert_edges(T, RT).

remove_duplicates([], []).
remove_duplicates([H|T], RT) :-
	revert_edges(H, RH),
	reverse(RH, RRH),
	memberchk(RRH, T),
	remove_duplicates(T, RT).
remove_duplicates([H|T], [H|RT]) :-
	remove_duplicates(T, RT).

% Main predicate for finding all possible Hamiltonian cycles
find_cycles(Res) :-
	% find starting predicate
	bagof(Y, Z^edge(Y, Z), R),
	R = [Start|_],
	% find cycles
	bagof(X, find_cycle(Start, X), DRes),
	% remove same cycles with different directions
	remove_duplicates(DRes, Res).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          Printing output                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

print_cycle([]):-
	write('\n').
print_cycle([A:B|T]) :-
	write(A),
	write('-'),
	write(B),
	write(' '),
	print_cycle(T).

print_output([]).
print_output([H|T]) :-
	print_cycle(H),
	print_output(T).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          Main predicate                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

main :-
	prompt(_, ''),
	read_lines(LL), !,
	upload_edges(LL), !,
	find_cycles(Res),
	print_output(Res),
	halt.
